const { Router } = require('express');
router = Router();

const {
  registerTransaction,
} = require('../controllers/payment-restaurant.controller');

const { requireRestaurantSignIn } = require('../helpers/payment.helper');

router.post(
  '/registertransaction',
  requireRestaurantSignIn,
  registerTransaction
);

module.exports = router;
