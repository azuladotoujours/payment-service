const expressJwt = require('express-jwt');
const dotenv = require('dotenv');
const dbQuery = require('../config');
dotenv.config();

const requireRestaurantSignIn = expressJwt({
  //if the token is valid, express jwt appends the verified admin id
  //in an auth key to the request object
  secret: process.env.RESTAURANT_SECRET,
  userProperty: 'authRestaurant',
});

module.exports = { requireRestaurantSignIn };
