const dbQuery = require('../config');
const moment = require('moment-timezone');

const registerTransaction = async (req, res) => {
  if (!req.body.amount || req.body.amount == 0) {
    return res.status(200).json({ error: 'Invalid amount' });
  }
  const { amount } = req.body;
  const method = 'LOCAL';
  const service = 'LOCAL';
  const restaurantId = req.authRestaurant.restaurantId;
  const date = moment(new Date());

  values = [restaurantId, method, date, service, amount];

  const { registerTransactionQuery } = require('../helpers/payment.querys');

  try {
    await dbQuery.query(registerTransactionQuery, values);
    res.status(200).json({ success: true });
  } catch (e) {
    res.status(500).json({ success: false });
  }
};

module.exports = { registerTransaction };
