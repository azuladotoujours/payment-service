CREATE DATABASE paymentmicroservice;

/*
Before the uuid, execute...

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

*/


CREATE TABLE payments (
    id uuid DEFAULT uuid_generate_v4 (),
    method VARCHAR(40) NOT NULL,
    date   TIMESTAMP     NOT NULL,
    service VARCHAR(40) NOT NULL,
    amount VARCHAR(40) NOT NULL,
    restaurant_id VARCHAR(40),
    contract_id     VARCHAR(40),

    PRIMARY KEY(id)
);